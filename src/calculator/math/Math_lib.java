package calculator.math;

import calculator.window.Java_Calculator;

public class Math_lib {

	// math library attributes
	private boolean number, operand, hascoma = false;
	private double var = 0;
	private String currentOperand = "";

	// addition
	public double add(double x, double y) {
		return x + y;
	}
	// subtraction
	public double sub(double x, double y) {
		return x - y;
	}
	// multiplication
	public double mul(double x, double y) {
		return x * y;
	}
	// division
	public double div(double x, double y) {
		return x / y;
	}

	// **
	public double pow(double x, double y) {
		return Math.pow(x, y);
	}

	// sqrt()
	public double squareroot(double x) {
		
		if (x < 0)
			Java_Calculator.calculatorLock = true;
		
		return Math.sqrt(x);
	}
	
	// !
	public double factorial(double x) {

		x = Math.floor(x);
		double tmp = 1;

		for (double tmp2 = x; tmp2 > 0; --tmp2) {
			tmp *= tmp2;
		}

		return tmp;

	}
	
	// 1 / x
	public double onedivx(double x) {
		return 1 / x;
	}

	// getters and setters
	public double getVar() {
		return var;
	}

	public void setNumber(boolean number) {
		this.number = number;
	}

	public boolean isNumber() {
		return number;
	}

	public boolean isOperand() {
		return operand;
	}

	public boolean isHascoma() {
		return hascoma;
	}

	public void setOperand(boolean operand) {
		this.operand = operand;
	}

	public void setVar(double var) {
		this.var = var;
	}

	public void setHascoma(boolean hascoma) {
		this.hascoma = hascoma;
	}

	public String getCurrentOperand() {
		return currentOperand;
	}

	public void setCurrentOperand(String currentOperand) {
		this.currentOperand = currentOperand;
	}
	// /getters and setters




}
