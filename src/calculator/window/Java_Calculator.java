package calculator.window;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.UIManager;

import calculator.math.Math_lib;

import java.awt.Color;

public class Java_Calculator {

	public JFrame frame;
	public JTextField textField;
	private Math_lib mathlib = new Math_lib();
	private JTextField textField_2;
	public static boolean calculatorLock = false;

	// initialize calculator on start
	public Java_Calculator() {
		initialize();
	}

	// clearing calculator display
	private void clearCalculator() {

		// clear display
		textField.setText("");
		textField_2.setText("");

		// reset calculator states
		mathlib.setVar(0);
		mathlib.setOperand(false);
		mathlib.setNumber(false);
		mathlib.setHascoma(false);
		
		// remove calculator lock if it was set
		calculatorLock = false;

	}
	
	// function that displays help to the display and locks the calculator
	private void showHelp() {
		
		textField_2.setText("IVS calculator 1.0 - made by team UTUK - press 'C' to continue");
		textField.setText("Input values and use math operands on the right for results");
		
		calculatorLock = true;
	}

	// support function to check whether a string can be parsed to double
	private boolean isNumeric(String str) {

		try {
		  Double.parseDouble(str);
		}
		catch (NumberFormatException nfe) {
			return false;
		}

		return true;

	}

	// check the double value to remove the dot
	private String checkDoubleFormat(double var) {

		String tmp = Double.toString(var);;

		if (var % 2 == 1 || var % 2 == 0)
			return tmp.substring(0, tmp.length() - 2);

		else
			return Double.toString(var);

	}

	// resolving and returning calculations
	private void calculatorResolve() {
		
		if (calculatorLock)
			return;

		double result = 0;

		if (!isNumeric(textField.getText().substring(textField.getText().length() - 1)))
			return;

		if (mathlib.getCurrentOperand() == "")
			return;
		else if (mathlib.getCurrentOperand() == "+") {
			result = mathlib.add(mathlib.getVar(), Double.parseDouble(textField.getText()));
			textField.setText(checkDoubleFormat(result));
		}
		else if (mathlib.getCurrentOperand() == "-") {
			result = mathlib.sub(mathlib.getVar(), Double.parseDouble(textField.getText()));
			textField.setText(checkDoubleFormat(result));
		}
		else if (mathlib.getCurrentOperand() == "*") {
			result = mathlib.mul(mathlib.getVar(), Double.parseDouble(textField.getText()));
			textField.setText(checkDoubleFormat(result));

		}
		else if (mathlib.getCurrentOperand() == "/") {
			result = mathlib.div(mathlib.getVar(), Double.parseDouble(textField.getText()));
			textField.setText(checkDoubleFormat(result));
		}
		else if (mathlib.getCurrentOperand() == "^") {
			result = mathlib.pow(mathlib.getVar(), Double.parseDouble(textField.getText()));
			textField.setText(checkDoubleFormat(result));
		}

		// editing second display
		textField_2.setText(String.valueOf(checkDoubleFormat(result)));

		mathlib.setCurrentOperand("");

		if (Double.parseDouble(textField.getText()) == Double.POSITIVE_INFINITY ||
			Double.parseDouble(textField.getText()) == Double.NEGATIVE_INFINITY)
			calculatorLock = true;
		
	}

	// inputting coma to calculator
	private void addComa() {

		String displayText = textField.getText();

		if (mathlib.isHascoma() == false && !displayText.equals("") && isNumeric(displayText.substring(displayText.length() - 1))) {
			textField.setText(textField.getText() + ".");
			mathlib.setHascoma(true);
		}

	}

	// fast mathematical operations
	private void fastMathOperations(String operand) {

		if (calculatorLock)
			return;
		
		String tmp = textField.getText();

		if (!isNumeric(tmp.substring(tmp.length() - 1)))
			textField.setText(tmp.substring(0, tmp.length() - 1));

		double displayValue = Double.parseDouble(textField.getText());

		if (operand == "\u221A") {
			displayValue = mathlib.squareroot(displayValue);
		}
		else if (operand == "1/x") {
			displayValue = mathlib.onedivx(displayValue);
		}
		else if (operand == "!") {
			displayValue = mathlib.factorial(displayValue);
		}

		// editing main display
		textField.setText(String.valueOf(checkDoubleFormat(displayValue)));

		// editing second display
		textField_2.setText(String.valueOf(checkDoubleFormat(displayValue)));

		if (Double.parseDouble(textField.getText()) == Double.POSITIVE_INFINITY ||
			Double.parseDouble(textField.getText()) == Double.NEGATIVE_INFINITY)
			calculatorLock = true;
		
	}

	// inputting arithmetic operands to calculator
	private void addMathOperand(String operand) {
		
		if (calculatorLock)
			return;

		// and then making additions to that negative number
		if (mathlib.getCurrentOperand() != "") {
			calculatorResolve();
		}

		// adding an operand for negative number
		if (operand == "-" && textField.getText().isEmpty()) {
			textField.setText(textField.getText() + "-");
			return;
		}

		if (mathlib.isOperand() == false && isNumeric(textField.getText().substring(textField.getText().length() - 1))) {
			mathlib.setVar(Double.parseDouble(textField.getText()));
			textField.setText(textField.getText() + operand);
			mathlib.setOperand(true);
		}

		else {
			textField.setText(textField.getText().substring(0, textField.getText().length() - 1));
			textField.setText(textField.getText() + operand);
			mathlib.setOperand(true);
		}

		mathlib.setCurrentOperand(operand);

	}

	// inputting numbers to calculator
	public void addNumber (String number) {

		if (calculatorLock)
			return;
		
		if (mathlib.isOperand()) {

			// editing second display
			if (!textField_2.getText().isEmpty())
				textField_2.setText(textField_2.getText() + mathlib.getCurrentOperand());
			else
				textField_2.setText(textField.getText());

			textField.setText("");
			mathlib.setOperand(false);
			mathlib.setHascoma(false);

		}

		textField.setText(textField.getText() + number);


	}

	// Initialize main frame contents
	public void initialize() {
		frame = new JFrame("IVS Java Calculator");
		frame.setResizable(false);
		frame.setBounds(100, 100, 386, 488);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		// button 1
		JButton button_1 = new JButton("1");
		button_1.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addNumber(button_1.getText());
			}
		});
		button_1.setBounds(10, 161, 64, 64);
		frame.getContentPane().add(button_1);

		// button 2
		JButton button_2 = new JButton("2");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addNumber(button_2.getText());
			}
		});
		button_2.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		button_2.setBounds(84, 161, 64, 64);
		frame.getContentPane().add(button_2);

		// button 3
		JButton button_3 = new JButton("3");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addNumber(button_3.getText());
			}
		});
		button_3.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		button_3.setBounds(158, 161, 64, 64);
		frame.getContentPane().add(button_3);

		// button 4
		JButton button_4 = new JButton("4");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addNumber(button_4.getText());
			}
		});
		button_4.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		button_4.setBounds(10, 236, 64, 64);
		frame.getContentPane().add(button_4);

		// button 5
		JButton button_5 = new JButton("5");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addNumber(button_5.getText());
			}
		});
		button_5.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		button_5.setBounds(84, 236, 64, 64);
		frame.getContentPane().add(button_5);

		// button 6
		JButton button_6 = new JButton("6");
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addNumber(button_6.getText());
			}
		});
		button_6.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		button_6.setBounds(158, 236, 64, 64);
		frame.getContentPane().add(button_6);

		// button 7
		JButton button_7 = new JButton("7");
		button_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addNumber(button_7.getText());
			}
		});
		button_7.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		button_7.setBounds(10, 311, 64, 64);
		frame.getContentPane().add(button_7);

		// button 8
		JButton button_8 = new JButton("8");
		button_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addNumber(button_8.getText());
			}
		});
		button_8.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		button_8.setBounds(84, 311, 64, 64);
		frame.getContentPane().add(button_8);

		// button 9
		JButton button_9 = new JButton("9");
		button_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addNumber(button_9.getText());
			}
		});
		button_9.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		button_9.setBounds(158, 311, 64, 64);
		frame.getContentPane().add(button_9);

		// button 0
		JButton button_0 = new JButton("0");
		button_0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addNumber(button_0.getText());
			}
		});
		button_0.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		button_0.setBounds(10, 386, 138, 64);
		frame.getContentPane().add(button_0);

		JButton button_10 = new JButton(",");
		button_10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addComa();
			}
		});
		button_10.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		button_10.setBounds(158, 386, 64, 64);
		frame.getContentPane().add(button_10);

		JButton button_11 = new JButton("+");
		button_11.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		button_11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!textField.getText().isEmpty())
					addMathOperand(button_11.getText());
			}
		});
		button_11.setBounds(232, 311, 64, 64);
		frame.getContentPane().add(button_11);

		JButton button_12 = new JButton("-");
		button_12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					addMathOperand(button_12.getText());
			}
		});
		button_12.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		button_12.setBounds(232, 236, 64, 64);
		frame.getContentPane().add(button_12);

		JButton button_13 = new JButton("*");
		button_13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!textField.getText().isEmpty())
					addMathOperand(button_13.getText());
			}
		});
		button_13.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		button_13.setBounds(232, 161, 64, 64);
		frame.getContentPane().add(button_13);

		JButton button_14 = new JButton("/");
		button_14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!textField.getText().isEmpty())
					addMathOperand(button_14.getText());
			}
		});
		button_14.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		button_14.setBounds(232, 86, 64, 64);
		frame.getContentPane().add(button_14);

		JButton btnC = new JButton("C");
		btnC.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				clearCalculator();

			}

		});
		btnC.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		btnC.setBounds(10, 86, 138, 64);
		frame.getContentPane().add(btnC);

		JButton button_18 = new JButton("=");
		button_18.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!textField.getText().isEmpty() && !textField_2.getText().isEmpty())
					calculatorResolve();
			}
		});
		button_18.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		button_18.setBounds(232, 386, 138, 64);
		frame.getContentPane().add(button_18);

		JButton button_19 = new JButton("?");
		button_19.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearCalculator();
				showHelp();
			}
		});
		button_19.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		button_19.setBounds(158, 86, 64, 64);
		frame.getContentPane().add(button_19);

		JButton button_20 = new JButton("\u221A");
		button_20.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!textField.getText().isEmpty())
					fastMathOperations(button_20.getText());
			}
		});
		button_20.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		button_20.setBounds(306, 161, 64, 64);
		frame.getContentPane().add(button_20);

		JButton button_16 = new JButton("^");
		button_16.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!textField.getText().isEmpty())
					addMathOperand(button_16.getText());
			}
		});
		button_16.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		button_16.setBounds(306, 86, 64, 64);
		frame.getContentPane().add(button_16);

		JButton button_21 = new JButton("!");
		button_21.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		button_21.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!textField.getText().isEmpty())
					fastMathOperations(button_21.getText());
			}
		});
		button_21.setBounds(306, 236, 64, 64);
		frame.getContentPane().add(button_21);

		JButton btnx = new JButton("1/x");
		btnx.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!textField.getText().isEmpty())
					fastMathOperations(btnx.getText());
			}
		});
		btnx.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		btnx.setBounds(306, 311, 64, 64);
		frame.getContentPane().add(btnx);

		JPanel panel = new JPanel();
		panel.setBorder(UIManager.getBorder("Button.border"));
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(10, 10, 360, 68);
		frame.getContentPane().add(panel);
		panel.setLayout(null);

		textField = new JTextField();
		textField.setBounds(2, 28, 356, 38);
		panel.add(textField);
		textField.setFont(new Font("Myriad Pro", Font.PLAIN, 14));
		textField.setEditable(false);
		textField.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setForeground(Color.GRAY);
		textField_2.setFont(new Font("Myriad Pro", Font.PLAIN, 12));
		textField_2.setEditable(false);
		textField_2.setColumns(10);
		textField_2.setBounds(2, 2, 356, 27);
		panel.add(textField_2);
	}
}
