// Projekt: IVS proj n. 2, Java Calculator
// Date: 1.2. 2017 2016 / 2017
// Authors: Ji�� Kabelka

package calculator;

import java.awt.EventQueue;

import calculator.window.Java_Calculator;

public class Launcher {

	// initialize the calculator
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
				
				try {
					Java_Calculator window = new Java_Calculator();
					window.frame.setVisible(true);
				} 
				
				catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		});
		
	}
	
}
